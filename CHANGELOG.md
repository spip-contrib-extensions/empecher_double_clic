# CHANGELOG
## 1.0.4 - 2024-05-07

### Fixed

- Compatiblité SPIP 4.y.z

### Removed

- Compatibilité SPIP 3.2.z

## 1.0.3 - 2023-02-27

### Fixed

- Compatibilité SPIP 4.2
