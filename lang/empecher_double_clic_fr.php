<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/en_travaux.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	// C 
	'configuration' => 'Configuration',
	
	// J
	'javascript_global' => 'Charger le javascript sur toutes les pages, dans la balise &lt;head&gt;'
	
);
